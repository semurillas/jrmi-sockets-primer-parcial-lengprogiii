/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

 import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServidorConsignaRMI {
    
    public static void main(String[] args) throws RemoteException {
		ImplementaConsigna implementaconsigna = new ImplementaConsigna();
		ConsignaRMI consignarmi = (ConsignaRMI) UnicastRemoteObject.exportObject(implementaconsigna, 0);
		
		Registry registry = LocateRegistry.createRegistry(1099);
		registry.rebind("RemoteOBJ", consignarmi);
	}
    
   
}
