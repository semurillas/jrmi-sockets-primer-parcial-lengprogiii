/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.rmi.Remote;

public interface RetiraRMI extends Remote {
    
    public int Transacciones()throws Exception;
    
    public void Retiro()throws Exception;
    
    public int getSaldo() throws Exception;
    
    public void setSaldo(int saldo) throws Exception;
    
    
}
