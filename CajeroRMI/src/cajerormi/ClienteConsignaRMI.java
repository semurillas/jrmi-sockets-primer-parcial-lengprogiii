/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClienteConsignaRMI {
    
    public static void main(String[] args) throws RemoteException, NotBoundException, Exception {
		Registry registry = LocateRegistry.getRegistry("127.0.0.1", 1099);
                ConsignaRMI consignarmi;
		
		consignarmi = (ConsignaRMI) registry.lookup("RemoteOBJ");
                
                
		consignarmi.Transacciones();

		
	}
      
}
