
package cajerormi;
import java.rmi.RemoteException;
import java.util.Scanner;


public class ImplementaConsigna implements ConsignaRMI {
    
    protected int deposito, retiro, transacciones;
    private static int saldo;
    
    
    
    Scanner sc = new Scanner(System.in);
    
    protected ImplementaConsigna(){
        super();
    }
    
    @Override
    public void Deposito() throws RemoteException{
        deposito = sc.nextInt();
    }
    @Override
    public int getSaldo(){
        return saldo;
    }
    
    @Override
    public void setSaldo(int saldo){
        this.saldo = saldo;
    }
    
    @Override
    public void Transacciones() throws RemoteException{
        
        System.out.print("Cuanto deseas depositar: ");
        Deposito();
        
        transacciones = getSaldo();
        setSaldo(transacciones + deposito);
        System.out.println("-----------------------------------");
        System.out.println("Depositaste: " + deposito);
        System.out.println("Tu saldo actual es: " + getSaldo());
        System.out.println("-----------------------------------");
    
    }
    
    

    
}




