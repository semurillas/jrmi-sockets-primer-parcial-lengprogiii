/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClienteCajero {

    public static void main(String[] args) throws Exception {
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        Socket socket = null;

        try {
            socket = new Socket("127.0.0.1", 12345);
            System.out.println("Openned connection");
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());

            oos.writeObject("Leonardo Campo");
            String message = (String) ois.readObject();
            System.out.println("\n" + message);
            
            
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            if (ois != null) {
                ois.close();
            }
            if (oos != null) {
                oos.close();
            }
            if (socket != null) {
                socket.close();
            }
            System.out.println("Closed connection");
        }

    }
}

