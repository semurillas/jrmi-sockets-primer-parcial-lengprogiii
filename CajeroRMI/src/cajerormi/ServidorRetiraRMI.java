/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author sebastian m
 */
public class ServidorRetiraRMI {
    
    public static void main(String[] args) throws RemoteException {
		ImplementaRetira implementaretira = new ImplementaRetira();
		RetiraRMI retirarmi = (RetiraRMI) UnicastRemoteObject.exportObject(implementaretira, 0);
		
		Registry registry = LocateRegistry.createRegistry(1090);
		registry.rebind("RemoteOBJ", retirarmi);
	}
    
}


