/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author sebastian m
 */
public class ClienteRetiraRMI {

    public static void main(String[] args) throws RemoteException, NotBoundException, Exception {

        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        Socket socket = null;
        ServerSocket serverSocket = new ServerSocket(12345);

        System.out.println("Started on port: 12345");

        Registry registry = LocateRegistry.getRegistry("127.0.0.1", 1090);
        RetiraRMI retirarmi;
        retirarmi = (RetiraRMI) registry.lookup("RemoteOBJ");

        while (true) {
            try {
                socket = serverSocket.accept();
                System.out.println("Connected from IP: " + socket.getInetAddress());
                ois = new ObjectInputStream(socket.getInputStream());
                oos = new ObjectOutputStream(socket.getOutputStream());

                /* String name = (String) ois.readObject();

                String msgClient = objectRMI.getRandom("Leonardo");
                System.out.println(msgClient);*/
                // oos.writeObject(msgClient);
                //String msgClient = retirarmi.Transacciones();
                // oos.writeObject(retirarmi.Transacciones());
                int transaction = (int) ois.readObject();

                int resultado = retirarmi.Transacciones();
                System.out.println(resultado);

                oos.writeObject(resultado);
                

            } catch (Exception ex) {
                ex.printStackTrace(System.err);
            } finally {
                if (ois != null) {
                    ois.close();
                }
                if (oos != null) {
                    oos.close();
                }
                if (socket != null) {
                    socket.close();
                }
                System.out.println("Closed connection");
            }

        }

    }

}
