/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.util.Scanner;

/**
 *
 * @author sebastian m
 */
public class ImplementaRetira implements RetiraRMI {
    protected int deposito, retiro, transacciones;
    private static int saldo;
    
    Scanner sc = new Scanner(System.in);
    
    protected ImplementaRetira(){
        super();
    }
    
    @Override
    public int getSaldo(){
        return saldo;
    }
    
    @Override
    public void setSaldo(int saldo){
        this.saldo = saldo;
    }
    
     @Override
    public int Transacciones() {
        System.out.print("Cuanto deseas retirar: ");
        Retiro();
        if (retiro <= getSaldo()) {
            transacciones = getSaldo();
            setSaldo(transacciones - retiro);
            System.out.println("-----------------------------------");
            System.out.println("Retiraste : " + retiro);
            System.out.println("Tu saldo actual es: " + getSaldo());
            System.out.println("-----------------------------------");
        } else {
            System.out.println("---------------------");
            System.out.println("Saldo insuficiente.");
            System.out.println("---------------------");
        }
        return retiro;
    }

    @Override
    public void Retiro(){
        retiro = sc.nextInt();
    }
}
