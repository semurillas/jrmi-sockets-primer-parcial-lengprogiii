/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerormi;

import java.rmi.Remote;

public interface ConsignaRMI extends Remote {
    
    public void Transacciones()throws Exception;
    
    public void Deposito()throws Exception;
    
    public int getSaldo() throws Exception;
    
    public void setSaldo(int saldo) throws Exception;
    
}
